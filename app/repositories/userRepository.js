const model = require('../models')

module.exports = {
    create(createAccount) {
        return model.user.create(createAccount);
    },

    finduser(user) {
        return model.user.findOne({
            where: {
                username: user
            }
        })
    },

    update(id, updateCar){
        return model.car.update(updateCar, {    
            where: {
                id : id
            },
        });
    },
}