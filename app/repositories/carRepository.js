const model = require('../models')

module.exports = {
    create(createCar){
        return model.car.create(createCar);
    },

    update(id, updateCar){
        return model.car.update(updateCar, {    
            where: {
                id : id
            },
        });
    },

    delete(id){
        return model.car.destroy({
            where: {
                id : id
            },
        });
    },

    find(id){
        return model.car.findOne(id);
    },

    findAll(){
        return model.car.findAll();
    },

}