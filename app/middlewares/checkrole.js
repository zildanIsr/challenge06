const checkRole = (req, res, next) => {
    const user = res.locals.user
    const role = user.role
    if (role !== 'admin' && role !== 'superadmin') {
        return res.status(403).json({
            "success" : false,
            "error" : 403,
            "message" : 'Access Deined',
            "role" : user.role
        })
    }

    next()
}

module.exports = checkRole
