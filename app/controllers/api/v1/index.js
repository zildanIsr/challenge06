const userController = require("./userController");
const carController = require("./carController");

module.exports = {
  carController,
  userController,
};
