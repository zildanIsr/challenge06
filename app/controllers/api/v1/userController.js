const userService = require('../../../service/userService');

module.exports = {
    register(req, res) {
        userService
            .create(req.body)
            .then(data => {
                res.status(200).json({
                    "success" : true,
                    "error" : 0,
                    "message" : "user successfully registered",
                    "data" : data
                });
            })
            .catch(err => {
                res.status(500).json({
                    "success" : false,
                    "error" : err.code,
                    "message" : err,
                    "data" : null
                });
            });
    },
    login(req, res) {
        userService
            .login(req.body)
            .then(data => {
                res.status(200).json({
                    "success" : true,
                    "error" : 0,
                    "message" : "user successfully login",
                    "data" : {
                        "token" : data.token
                    }
                });
            })
            .catch(err => {
                res.status(500).json({
                    "success" : false,
                    "error" : err.code,
                    "message" : err,
                    "data" : null
                });
            }
        );
    },
    addAdmin: async (req, res) => {
        try {
            const admin = await userService.update(req.params.id, req.body)
            
            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "user successfully updated",
                "data" : admin
            });
        } catch (err) {
            return res.status(500).json({
                "success" : false,
                "error" : err.code,
                "message" : err,
                "data" : null
            });
        }
    }
}

