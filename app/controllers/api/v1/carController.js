const carService = require('../../../service/carService');

module.exports = {
    create(req, res) {
        const user = res.locals.user
        carService
            .create(req.body)
            .then(data => {
                res.status(200).json({
                    "success" : true,
                    "error" : 0,
                    "message" : "car successfully created",
                    "data" : data,
                    "createdBy" : user.username
                });
            })
            .catch(err => {
                res.status(500).json({
                    "success" : false,
                    "error" : err.code,
                    "message" : err,
                    "data" : null
                });
            });
    },

    list(req, res) {
        carService
            .list()
            .then(data => {
                res.status(200).json({
                    "success" : true,
                    "error" : 0,
                    "message" : "car successfully listed",
                    "data" : data
                });
            })
            .catch(err => {
                res.status(500).json({
                    "success" : false,
                    "error" : err.code,
                    "message" : err,
                    "data" : null
                });
            });
    },

    update(req, res) {
        const user = res.locals.user
        carService
            .update(req.params.id, req.body)
            .then(() => {
                res.status(200).json({
                    "success" : true,
                    "error" : 0,
                    "message" : "car successfully updated",
                    "updatedBy" : user.username
                });
            })
            .catch(err => {
                res.status(500).json({
                    "success" : false,
                    "error" : err.code,
                    "message" : err,

                });
            });
    },

    delete(req, res) {
        carService
            .delete(req.params.id)
            .then(() => {
                res.status(200).json({
                    "success" : true,
                    "error" : 0,
                    "message" : "car successfully deleted",
                });
            })
            .catch(err => {
                res.status(500).json({
                    "success" : false,
                    "error" : err.code,
                    "message" : err,
                });
            });
    },

}