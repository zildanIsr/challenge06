const { body } = require('express-validator')

const createCarRules = [
    body('name').notEmpty().withMessage('Name is required'),
    body('price').notEmpty().withMessage('Price is required'),
    body('size').notEmpty().withMessage('Size is required'),
]

const createUserRules = [
    body('email').isEmail().withMessage('email invalid').notEmpty().withMessage('email is required'),
    body('username').notEmpty().withMessage('username is required'),
    body('firstname').notEmpty().withMessage('firstName is required'),
    body('lastname').notEmpty().withMessage('lastName is required'),
    body('password').notEmpty().withMessage('password is required')
]

module.exports = {
    createUserRules,
    createCarRules
}