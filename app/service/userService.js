const userRepository = require('../repositories/userRepository'),
    { genSalt, hash, compareSync } = require('bcrypt'),
    jwt = require('jsonwebtoken')

const cryptPassword = async (password) => {
    const salt = await genSalt(12)
    
    return hash(password, salt)
}

module.exports = {
    async create(requestBody){
        try {
            const user = await userRepository.create({
                ...requestBody,
                password: await cryptPassword(requestBody.password)
            })
    
            return {
                data : user,
            }
        } catch (error) {
            throw error;
        }
    },

    async login(requestBody){
        try {
            const userExists = await userRepository.finduser(requestBody.username)
            
            if(!userExists)
                return {
                    message : 'User not found',
                    data : null,
                }
            
            if (compareSync(requestBody.password, userExists.password)) {

                const token = jwt.sign(
                    { id: userExists.id, username: userExists.username, email: userExists.email, role: userExists.role},
                    'password!23',
                    { expiresIn: '12h' }
                )

                return {
                    message : 'Login success',
                    token :  token
                }
            }

            return {
                message : 'invalid credentials',
                data : null,
            }
        } catch (error) {
            throw error;
        }
    },

    update: (id, requestBody) => userRepository.update(id, requestBody),

}