const express = require('express');
const userController = require('../app/controllers')
const validate = require('../app/middlewares/validate')
const { createUserRules } = require('../app/validators/rules')

const router = express.Router();

router.post('/register', validate(createUserRules) , userController.api.v1.userController.register);
router.post('/login', userController.api.v1.userController.login);
router.put('/admin/:id', userController.api.v1.userController.addAdmin);


module.exports = router;