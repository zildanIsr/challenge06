const express = require('express')
const carController = require('../app/controllers')
const validate = require('../app/middlewares/validate')
const checkToken = require('../app/middlewares/checkToken')
const checkRole = require('../app/middlewares/checkrole')
const { createCarRules } = require('../app/validators/rules')

const router = express.Router()

router.get('/list', checkToken, carController.api.v1.carController.list);
router.post('/create', checkToken, checkRole, validate(createCarRules), carController.api.v1.carController.create);
router.put('/update/:id', checkToken, checkRole, carController.api.v1.carController.update);
router.delete('/delete/:id', checkToken, checkRole, carController.api.v1.carController.delete);


module.exports = router;