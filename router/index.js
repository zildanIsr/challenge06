const express = require('express')
const userRouter = require('./user')
const carRouter = require('./car')
const controllers = require('../app/controllers')

const router = express.Router()

router.get('/check-healty', (req, res) => res.send("aplication up"))
router.use('/user', userRouter)
router.use('/car', carRouter)

router.use(controllers.api.main.onLost);
router.use(controllers.api.main.onError);

module.exports = router 